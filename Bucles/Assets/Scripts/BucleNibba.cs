using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BucleNibba : MonoBehaviour
{
    public int numeroDeCuadrados;
    public int multiplicador;
    public GameObject objetodistancia;
    public GameObject nibbadistancia;
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < numeroDeCuadrados; i++)
        {
            Instantiate(objetodistancia, multiplicador * i * new Vector2(2, 0) + new Vector2 (1,0), Quaternion.identity);
        }
        for (int i = 0; i < numeroDeCuadrados; i++)
        {
            Instantiate(nibbadistancia, multiplicador * i * new Vector2(2, 0), Quaternion.identity);
        }
        for (int i = 0; i < numeroDeCuadrados; i++)
        {
            Instantiate(objetodistancia, multiplicador * i * new Vector2(0, 2) + new Vector2(0,1), Quaternion.identity);
        }
        for (int i = 0; i < numeroDeCuadrados; i++)
        {
            Instantiate(nibbadistancia, multiplicador * i * new Vector2(0, 2), Quaternion.identity);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
