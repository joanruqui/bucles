using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PruebaBuclesDoWhile : MonoBehaviour
{
    public int contador;

    private void Start()
    {
        /* En los bucles do...while, debemos incluir la siguiente configuraci�n:
         * int i = __ --> Se declara la variable que utilizamos para la iteraci�n antes de empezar el bucle
         * do { --> Indica el inicio de las instrucciones que se repetir�n en el interior del bucle
         * i++ --> En el interior del bucle, se indica el incremento de la variable en cada una de las iteraciones
         * while (i <= __); --> Se indica la condici�n que debe cumplirse para seguir ejecut�ndose el bucle         
         */

        // Los dos bucles siguientes se ejecutan 50 veces, mostrando por tanto 50 veces "hola" cada uno
        int i = 0;
        do
        {
            Debug.Log("hola");
            i++;
        } while (i < 50);
        int j = 1;
        do
        {
            Debug.Log("hola");
            j++;
        } while (j <= 50);

        // Los dos bucles siguientes se ejecutan las veces indicadas en la variable "contador"
        // Atenci�n al if para comprobar que la variable "contador" contenga un n�mero mayor que 0, ya que los bucles do...while siempre se ejecutan al menos una vez
        if (contador > 0)
        {
            int k = 0;
            do
            {
                Debug.Log("hola");
                k++;
            } while (k < contador);
        }
        if (contador > 0)
        {
            int l = 1;
            do
            {
                Debug.Log("hola");
                l++;
            } while (l <= contador);
        }

        // El siguiente bucle imprime por consola los n�meros del 1 al 30
        int m = 1;
        do
        {
            Debug.Log(m);
            m++;
        } while (m <= 30);
        // El siguiente bucle imprime por consola los n�meros del 1 al n�mero que contenga la variable "contador"
        // Atenci�n al if para comprobar que la variable "contador" contenga un n�mero mayor que 0, ya que los bucles do...while siempre se ejecutan al menos una vez
        if (contador > 0)
        {
            int n = 1;
            do
            {
                Debug.Log(n);
                n++;
            } while (n <= contador);
        }

        // El siguiente bucle imprime por consola los n�meros impares menores que el n�mero que contanga la variable "contador"
        // Atenci�n al if para comprobar que la variable "contador" contenga un n�mero mayor que 0, ya que los bucles do...while siempre se ejecutan al menos una vez
        if (contador > 0)
        {
            int o = 1;
            do
            {
                if (o % 2 != 0)
                {
                    Debug.Log(o);
                }
                o++;
            } while (o <= contador);
        }
    }
}